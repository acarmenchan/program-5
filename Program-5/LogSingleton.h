//
//  LogSingleton.h
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#ifndef __Program_5__LogSingleton__
#define __Program_5__LogSingleton__

#include "CS1337Logger.h"
#include "ScreenLogger.h"
#include "FileLogger.h"
#include <iostream>

using namespace std;

class LogSingleton
{
private:
    static CS1337Logger *logger;
    
    //private constructor & destructor
    LogSingleton();
    ~LogSingleton();
    
public:
    static CS1337Logger* GetLogger();
};


#endif /* defined(__Program_5__LogSingleton__) */
