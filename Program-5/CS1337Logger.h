//
//  CS1337Logger.h
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#ifndef __Program_5__CS1337Logger__
#define __Program_5__CS1337Logger__

#include <iostream>

using namespace std;

//abstract base class
class CS1337Logger
{
private:
    bool loggingEnabled;
    
protected:
    //pure virtual function - makes this class an abstract class
    virtual void displayMessage(const char*) = 0;
    
public:
    //constructor
    CS1337Logger();
    
    //virtual destructor
    virtual ~CS1337Logger() {}
    
    //setters
    void setLoggingEnabled(bool _loggingEnabled)
    {   loggingEnabled = _loggingEnabled;      }
    
    //getters
    bool getLoggingEnabled() const
    {   return loggingEnabled;      }
    
    //methods
    void logMessage(const char*);
};

#endif /* defined(__Program_5__CS1337Logger__) */
