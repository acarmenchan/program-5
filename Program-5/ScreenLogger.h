//
//  ScreenLogger.h
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#ifndef __Program_5__ScreenLogger__
#define __Program_5__ScreenLogger__

#include "CS1337Logger.h"
#include <iostream>

using namespace std;

//base class
class ScreenLogger: public CS1337Logger
{
private:
    ~ScreenLogger() {}
    
public:
    //must override displayMessage func in base class with the result that the message is displayed to the screen
    void displayMessage(const char*);
};

#endif /* defined(__Program_5__ScreenLogger__) */
