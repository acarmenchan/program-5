//
//  LogSingleton.cpp
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#include "LogSingleton.h"

CS1337Logger* LogSingleton::logger = new FileLogger();
//CS1337Logger* LogSingleton::logger = new ScreenLogger();

LogSingleton::~LogSingleton()
{
    delete logger;
}

CS1337Logger* LogSingleton::GetLogger()
{
    return logger;
}