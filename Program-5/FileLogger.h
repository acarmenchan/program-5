//
//  FileLogger.h
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#ifndef __Program_5__FileLogger__
#define __Program_5__FileLogger__

#include "CS1337Logger.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

//base class
class FileLogger: public CS1337Logger
{
private:
    ofstream fout;
    string outputFileName = "Log.txt";
    
    ~FileLogger();  //defined in implementation file
    
public:
    FileLogger();
    //must override displayMessage func in base class with the result that the message is displayed BOTH to screen and to a log file with fixed name "Log.txt"
    void displayMessage(const char*);
};

#endif /* defined(__Program_5__FileLogger__) */
