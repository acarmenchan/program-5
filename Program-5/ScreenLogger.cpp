//
//  ScreenLogger.cpp
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#include "ScreenLogger.h"

void ScreenLogger::displayMessage(const char *message)
{
    cout << message << endl;
}