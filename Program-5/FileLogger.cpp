//
//  FileLogger.cpp
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#include "FileLogger.h"

FileLogger::FileLogger()
{
    fout.open(outputFileName);
}

FileLogger::~FileLogger()
{
    fout.close();
}

void FileLogger::displayMessage(const char *message)
{
    cout << message << endl;
    fout << message << endl;
    fout << endl;
}