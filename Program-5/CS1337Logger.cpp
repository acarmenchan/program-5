//
//  CS1337Logger.cpp
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#include "CS1337Logger.h"

CS1337Logger::CS1337Logger()
{
    loggingEnabled = false;
}

void CS1337Logger::logMessage(const char * message)
{
    if (loggingEnabled)     // if (loggingEnabled == true)
    {
        displayMessage(message);
    }
}