//
//  main.cpp
//  Program-5
//
//  Copyright (c) 2014 Carmen Chan. All rights reserved.
//

#include "CS1337Logger.h"
#include "ScreenLogger.h"
#include "FileLogger.h"
#include "LogSingleton.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void function1()
{
    LogSingleton::GetLogger()->logMessage("Entering function 1...");
    LogSingleton::GetLogger()->logMessage("Exiting function 1...");
}

void function2()
{
    LogSingleton::GetLogger()->logMessage("Entering function 2...");
    LogSingleton::GetLogger()->logMessage("Exiting function 2...");
}

void function3()
{
    LogSingleton::GetLogger()->logMessage("Entering function 3...");
    LogSingleton::GetLogger()->logMessage("Exiting function 3...");
}

void function4()
{
    LogSingleton::GetLogger()->logMessage("Entering function 4...");
    LogSingleton::GetLogger()->logMessage("Exiting function 4...");
}

void function5()
{
    LogSingleton::GetLogger()->logMessage("Entering function 5...");
    LogSingleton::GetLogger()->logMessage("Exiting function 5...");
}

int main(int argc, const char * argv[])
{
    //ENABLE
    cout << "Turning on logging..." << endl;
    LogSingleton::GetLogger()->setLoggingEnabled(true);
    LogSingleton::GetLogger()->logMessage("THIS WILL LOG!");
    cout << endl;
    
    //DISABLE
    cout << "Turning off logging..." << endl;
    LogSingleton::GetLogger()->setLoggingEnabled(false);
    LogSingleton::GetLogger()->logMessage("YOU CAN'T SEE ME");
    cout << endl;
    
    //RANDOM TURN OFF OR ON
    cout << "Enable or disable logging determined from random number generator % 2..." << endl;
    srand(time(NULL));
    int randomNumber = (rand() % 9) + 1;
    LogSingleton::GetLogger()->setLoggingEnabled(randomNumber % 2);
    LogSingleton::GetLogger()->logMessage("WILL NOW CALL MAIN FUNCTIONS!");
    cout << endl;
    
    //CALLING FUNCTIONS TO LOG ENTRY AND EXIT
    function1();
    function2();
    function3();
    function4();
    function5();
    
    cout << endl;
    return 0;
}

